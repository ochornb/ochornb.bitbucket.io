// JSON
//var recipes = '[{"rname":"Grandma\'s Hashbrowns","ingredients":"8 large russet potatoes, peeled"]';

//var tmpData = JSON.parse(recipes);

// CREATE THE STARTER RECIPE CARDS AND DISPLAY THEM
var recipes = [
    {key : 'bbqsteak', title : 'BBQ Steak', ingredients: "8 large russet potatoes, peeled<br> 4 tablespoons butter <br> 1 tablespoon salt<br>1 teaspoon ground black pepper", instructions: "1. In a large saucepan, cover potatoes with water. Bring water to a boil and cook until tender, about 20 minutes. Let cool in refrigerator overnight.\n\n2. The next morning, heat butter in a large skillet or frying pan. Shred potatoes and add to pan. Season with salt and pepper. Cook until golden brown on bottom, flip and brown on the other side.", imageURL: "https://images.media-allrecipes.com/images/55638.jpg"},
    {key : 'easychorizostreettacos', title : 'Easy Chorizo Street Tacos', ingredients: "1 chorizo sausage link, casing removed and meat crumbled\n 2 tablespoons chipotle peppers in abodo sauce\n 4 eaches corn tortillas\n 2 tablespoons chopped onion, or to taste\n 2 tablespoons chopped fresh cilantro, or to taste", instructions: "1. Mix chorizo and chipotle peppers together in a bowl.\n 2. Heat a skillet over medium hight heat; add chorizo and cook until crisp, 5 to 7 minutes. Transfer chorizo to a plate, reserving grease in the skillet\n\nHeat tortillas in the reserved grease in skillet over medium heat until warmed, 1 to 2 minutes per side. Stack 2 tortillas on top of each other for each taco. Fill with chorizo, onion, and cilantro.", imageURL: "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F4504506.jpg"},
    {key : 'orangefigandgorgonzolasalad', title : 'Orange, Fig, and Gorgonzola Salad', ingredients: "2 heads romaine lettuce, chopped\n 2 oranges-peeled, pith removed, and cut into segments\n 1/2 cup crumbled Gorgonzola cheese\n 2 eaches fresh figs, cut into 1-inch cubes\n 1/4 cup vinagrette dressing, or to taste", instructions: "Combine lettuce, oranges, Gorgonzola cheese, and figs in a large bowl. Drizzle dressing over salad and toss to coat.", imageURL: "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F43%2F2020%2F04%2F01%2FOrange-Fig-and-Gorgonzola-by-Christina-2000.jpg"},
    {key : 'shrimpscampi', title : 'Shrimp Scampi', ingredients: "Shrimp\n\n Butter", instructions: "Cook", imageURL: "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F5860407.jpg"}
]

// Populate with current cards
var cards = ''
for (var i = 0; i < recipes.length; i++) {
    cards += createCard(recipes[i])
};

$('#cards').html(cards);

// MODALS
$('#recipeModal').on('show.bs.modal', function (event){

    // GET THE RECIPE INDEX OR NAME
    // FIND RECIPE IN JSON FILE
    // POPULATE MODAL
    var button = $(event.relatedTarget);
    //var recipient = button.data('key'); // which button was pushed? use this to find in JSON array
    var recipient = button.data('key');
    // Grab the recipe, we'll use the data-recipe title to find it
    let recipe = recipes.find(x => x.key == recipient);

    const ingredientsRegex = /\n/g;
    recipe.ingredients = recipe.ingredients.replace(ingredientsRegex, '<br>')
    recipe.instructions = recipe.instructions.replace(ingredientsRegex, '<br>')
  

    var modal = $(this);
    modal.find('.modal-title').text(recipe.title);
    modal.find(".img-modal").attr("src", recipe.imageURL);
    modal.find('#ingredients').html(recipe.ingredients);
    modal.find('#instructions').html(recipe.instructions);
    
    // I've hit a skill wall in breaking a paragraph into smaller strings to format the recipes nicer
    // I can't find anything besides Regex, but what do I filter to get the result I want? Some instructions have more than one period in their steps
    
})

$('#recipeModal').on('hidden.bs.modal', function (event) {

})

$('#addRecipeModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var recipient = button.data('add')
    var modal = $(this)
});

$('#add-recipe-btn').on('click', function (event) {
    event.preventDefault();
    var title = $('#recipeTitle').val();
    var ingredients = $('#recipeIngredients').val();
    var instructions = $('#recipeInstructions').val();
    var imgURL = $('#recipeImg').val();

    // add to the recipes array
    recipes[recipes.length] = {key: keyify(title), title: title, ingredients: ingredients, instructions: instructions, imageURL: imgURL}
    
    // add a card
    $('#cards').append(createCard(recipes[recipes.length-1]))

    // close the modal
    // modal is closed using bootstrap data-dismiss: modal

    // clear the form data
    $('form').trigger('reset');

    
});

function createCard(recipesObject){
    
    var card = ""
    card += '<div data-key="' + recipesObject.key +'" class="col mb-4 d-flex flex-column align-items-stretch">';
    card += '<a class="card stretched-link text-decoration-none" href= "#" data-recipe ="' + recipesObject.title+ '" data-key = "' + recipesObject.key + '"data-toggle = "modal" data-target = "#recipeModal">';
    card += '<img src="'+ recipesObject.imageURL+'" alt="picture of " class="card-img-top">';
    card += '<div class="card__body">';
    card += '<div class="card-title-container mb-4 d-flex align-items-center">';
    card += '<h4 class="card-title ml-4 my-2">' + recipesObject.title +'</h4>';
    card += '</div>';
    card += '</div>';
    card += '<div class="card-footer">';
    card += '<button class="btn btn-primary" data-recipe = "' + recipesObject.title +'"  data-toggle = "modal" data-target = "#recipeModal"><i class="fas fa-plus-square mr-2"></i>OPEN RECIPE</button>';
    card += '</div>';
    card += '</a>';
    card += '</div>';

    return card;
}

$('#delete-btn').on('click', function(event){

    // Delete the recipe card and object info
    // by using the recipe's key
    // which is a stripped down version of the title
    let key = keyify($('#recipeModalLabel').text());

    // remove from recipe object array
    recipes = $.grep(recipes,
                        function(o,i) {return o.key === key;},
                        true);

    // Remove the card 
    var removeEl = $("div [data-key = " + key +"");
    $(removeEl).remove();
});

function keyify (inputString) {
    // using a numbered key they way I'm using it will cause problems
    // as deleting entries will mess up the current formula
    // causing duplicate keys (which both will be deleted)
    // use the recipe's title as a key, remove spaces and make it lowercase
    return inputString.toLowerCase().replace(/(\,+)*(\s+)*/g, '');
}

let editToggle = false;
let currentKey = "";

$('#recipeModal').on('hidden.bs.modal', function(e){
    // in case the user leaves via clicking outside the modal
    // turn off editing the modal or bad things will happen
    editToggle = false;
    $("#recipeImageEdit").remove();

    //reset the edit button's appearance
    $("#edit-btn").html('<i class="fas fa-edit"></i> EDIT')
})
$('#edit-btn').on('click', function(event){

    if (editToggle == false){
        currentKey =  keyify($('#recipeModalLabel').text());
        let currentLabel = $('#recipeModalLabel').text();
        let currentIngredients = $("#ingredients").html();
        currentIngredients = currentIngredients.replace(/(<br>)/g, '\n');
        let currentInstructions = $("#instructions").html();
        currentInstructions = currentInstructions.replace(/(<br>)/g, '\n');
        let currentImg = $('.img-modal').attr('src');
       
        let newLabel =          '<div class="form-group">'+
                                '<input type="text" class = "form-control bg-accent-light text-light" id = "recipeModalLabelEdit" value = "'+ currentLabel+'">'+
                                '</div>';
        let newIngredients =    '<div class="form-group">' +
                                '<textarea id="recipeIngredientsEdit"  rows="3" class="form-control bg-accent-light text-light">' + currentIngredients + '</textarea>' +
                                '</div>';
        let newInstructions =   '<div class="form-group">' +
                                '<textarea id="recipeInstructionsEdit"  rows="3" class="form-control bg-accent-light text-light">'+ currentInstructions + '</textarea>' +
                                '</div>';
        let newImage =          '<div class="form-group">' +
                                '<input type = "text" id="recipeImageEdit" class="form-control bg-accent-light text-light" value ="'+ currentImg + '"></input>' +
                                '</div>';

        $('#recipeModalLabel').html(newLabel);
        $('#ingredients').html(newIngredients);
        $('#instructions').html(newInstructions);
        $('#img-col').append(newImage);

        editToggle = true;

        // change edit button to a save button?
        $("#edit-btn").html('<i class="fas fa-save"></i> SAVE')

    } else{
        // Get the value of the edits
        let getChangeTitle = $("#recipeModalLabelEdit").val();
        let getChangeIngredients = $("#recipeIngredientsEdit").val();
        getChangeIngredients = getChangeIngredients.replace(/\n/g , '<br>')
        let getChangeInstructions = $("#recipeInstructionsEdit").val();
        getChangeInstructions = getChangeInstructions.replace(/\n/g , '<br>')
        let getChangeImage = $('#recipeImageEdit').val();

        $("#recipeModalLabel").text(getChangeTitle);
        $("#ingredients").html(getChangeIngredients);
        $("#instructions").html(getChangeInstructions);
        $("#img-col img").attr('src', getChangeImage);
    
        //get rid of the inputs
        $('#recipeModalLabelEdit').remove();
        $('#recipeIngredientsEdit').remove();
        $('#recipeInstructionsEdit').remove();
        $('#recipeImageEdit').remove();

        //update the object in recipes
        let recipeIndex = recipes.findIndex(recipe => recipe.key == currentKey);
        console.log("Recicpe Index  " + recipeIndex);

        if (recipeIndex !== -1){
            recipes[recipeIndex] = {key : keyify(getChangeTitle), title : getChangeTitle, ingredients: getChangeIngredients, instructions: getChangeInstructions, imageURL: getChangeImage};
             // update the card
            // When the edit button is clicked
            // Set title ingredients, instructions, and image url to editable
            $('div[data-key=' + currentKey + ']').replaceWith(createCard(recipes[recipeIndex]));
        }
        
        editToggle = false;
        // change the edit button back
        $("#edit-btn").html('<i class="fas fa-edit"></i> EDIT')
    }
   

});


